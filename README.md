## Håstival - Music festival Application for Android written in Kotlin

Learning project to start understanding mobile applications with the main goal being - to create an app for the Hastival festival which is on June 3rd to 5th day.

Project was developed during PTI education as a 2nd semester last project in association with a customer and contains a development and business plan with milestones.

Markdown (.md) files written in [ghostwriter](https://wereturtle.github.io/ghostwriter/)

Project IDE - [Android Studio](https://developer.android.com/studio)

Project is equipped with [GitLab pipeline](.gitlab-ci.yml) which performs automatic testing and deploying of the application with each commit and confirms working status.

### Introduction to the project

![Håstival Logo](/requirements/images/hastival_logo.png)

Håstival is a 3-day local music festival held in Fyn (Denmark) during Pinse (3-5 June, 2022).

First thoughts about the project, after first customer meeting:

The festival organizers would like to have an app for attendees to use that will show a schedule for the bands,  
including information and links to the bands own social media. The app could include a map of the festival grounds,  
other activities, and a section for buying from the bars and food trucks by linking to payment services.  
There would also be a way for attendees to upload their own pictures to a central database for sharing on Håstival  
social media platforms. Entrance tickets could also be purchased on the app.

## Android application version preview

Project v1:  
![Version 1 preview](/requirements/images/version1_app.png)

***

Project v2:  
![Version 2 preview](/requirements/images/version2_app.png)

## Repository format

Root folder of the repository contains all the required project files, [Documentation folder](/documentation) can be safely ignored.

[PDS folder](/documentation/2_Product_Planning) - the project's Product Design Specifications folder.

[Project brief folder](/documentation/1_Research&Design) - the project's brief folder.

[Documentation folder](/documentation) - relevant documentation of the project, not related to the application development itself.

[Requirements folder](/requirements) - project's images and other required resources.

[Customer meeting folder](/documentation/Customer_Meetings) - Customer meeting documentation.

[Group meeting folder](/documentation/Group_Meetings) - Group meeting documentation.

### Customer requirements

* We need map location of the festival,
* Band profile links,
* Ticket buying system through mobile-pay,
* Graphical interface design that fits a Nordic/Hastival vibe,
* Festival programme in the app,
* Pictures of the festival area and facilities,
* Locations of nearest non-festival shops,
* Main language of the app - Danish, possible change to English,
* Snack/drink options shown in the app and where to buy them,
* Overview/map of the festival area,

* Specific requirement - A reminder system - that one of the band that was favorited in the phone app is going to play soon,
only one stage is going to be present at the festival but the reminders should still be useful,\
there is a dj place somewhere else than the stage as well.

### Implemented feature list

* Main Activity containing:
	* Fragments (Screen tabs)
	* Bottom navigation
* Screen tabs (Fragments) containing:
	- Home Screen
		+ Logo of the Festival
		+ Relevant welcoming pictures
	- Schedule Screen
		+ Artist information
		+ Artist links
	- Contact Screen
		+ Ticket buying link
		+ Facebook link
	- Map Screen
		+ Markers of relevant locations
		+ Parking location
	- Practical festival information Screen
		+ Travelling options to/from the festival
		+ Food options
		+ Camping information and location
* Main application language - Danish
	- Implementing localization to English soon (Not a priority)
* Vertical screen scrolling inside tabs
* Horizontal screen scrolling

### Testing the project

In order to test the project, clone the repository and run the [.apk]() file in an Android emulator like [Blue Stacks](https://www.bluestacks.com/) or just on an Android device.

***

[Interview raw data](documentation\4_Testing\Hastival_interviews.docx) - The live qualitative interview data

### Developer contact information

Gytis V. - gytis.valatkevicius@gmail.com

Miksa S. - miksa.szeky@gmail.com

### Customer information

[Customer website](https://www.haastival.dk/)

* Festival location:  
Håstival  
Trekanten 2, Håstrup  
5600 Fåborg  

* Festival contacts:  
Maria Lundgaard  
Tlf. 22999712  
haastival@gmail.com  

