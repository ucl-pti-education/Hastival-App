Project Work:
			1.As a group, choose one of the problem areas described above to work with.
done			2.Decide on your prevalent theoretical approach and methodology for use in the project. [ we are choosing agile and gitlab development in agile methodology ]
			3.Use market research in several stages for your product to define specifications and enhance your product (Research competition, customer base, your own market strategy, then use that to test your prototype at every stage of development).
done			[For customer base we are pretty clear because it's going to be mostly Danish people, of various ages coming to listen to music in a local festival]
done			[We have researched competition in the market and have some examples of how they developed their applications]
issue			[Our own market strategy - to be done, issue in gitlab]
issue			4.Use quantitative and qualitative research methods for primary data collection and use secondary data information for comparative reasons or when primary data is not an option.
issue			5.Use the tools you learned from Value Proposition design to further improve your product concept.
gitlab milestones	6.Work through all the stages of design, product planning, prototyping, and testing as a group.
agile methodology	7.Manage your project work using the tools from project management.
issue			8.Make a business plan applying the concepts from Economics