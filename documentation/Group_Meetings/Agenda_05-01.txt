Members present: Gytis, Miksa
Meeting called by: Gytis, scheduled
Meeting time: 01/05/2022, 16:00
Topic: Continuation of the project, finalizing PDS
Agenda:

Creating program diagram
-Specify the layout of the program in a flowchart, including relations between components and
finalizing the naming scheme

Working on the software:
Development has shifted into creating a deployable client, this includes a shift from HTML to Java

Deciding parameters on project
-Specify all the remaining parameters necessary for starting the main development, including missing data
from the PDS

Fill out PDS with specifications
-Finalize the PDS to be presentable on 2nd of May.

Discussion over the PDS and the direction of the project




