#Market competitors and options for development

There are numerous festival apps and some of them we are going to take as inspiration/guidelines to make our app.  

One of the competitors that we have been researching on is "Radistai Village" - an original electronic music festival  
that happens in Lithuania during the summer. They have a phone app as well that we are going to take as inspiration.  

village.radistai.lt-v3.5.apk is the file of their app from 2019 and we can take it as a guideline for our app.  

Both of the apps are designed for music festivals so they can be used to fill each other in.  

##Competitor features and usage

They are not our direct competitors but we can use their developed features as guidelines and their mistakes as things  
we should avoid doing in order to achieve a succesful app development process.

The competitors provide various features in their apps, most of them are the requirements for our app as well.  

###Competitor info

Festival name - Radistai Village  
Location - Lithuania  
App name - Radistai Village 2019  