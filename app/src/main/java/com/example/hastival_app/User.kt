package com.example.hastival_app

data class User(var artistName : String, var artistDescription : String, var scheduleTime : String, var artistImage : Int)
