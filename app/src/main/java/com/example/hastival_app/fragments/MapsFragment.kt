package com.example.hastival_app.fragments

import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import com.example.hastival_app.R

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsFragment : Fragment() {

    private val callback = OnMapReadyCallback { googleMap ->

        val festival = LatLng(55.17476581207008, 10.200032098356381)
        googleMap.addMarker(MarkerOptions().position(festival).title("Håstival"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(festival))

        val festivalArea = LatLng(55.175234, 10.200208)
        googleMap.addMarker(MarkerOptions().position(festivalArea).title("Festivalområdet"))

        val festivalTent = LatLng(55.175528, 10.200323)
        googleMap.addMarker(MarkerOptions().position(festivalTent).title("Festivalcampingen"))

        val festivalShop = LatLng(55.17379434693385, 10.196121476340892)
        googleMap.addMarker(MarkerOptions().position(festivalShop).title("Nærmeste butik"))

        val festivalParking = LatLng(55.173090, 10.199822)
        googleMap.addMarker(MarkerOptions().position(festivalParking).title("Festival parkering"))

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

    }
}