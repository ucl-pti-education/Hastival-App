package com.example.hastival_app.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.hastival_app.MainActivity
import com.example.hastival_app.R
import com.example.hastival_app.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        val button = binding.ticketButton
        button.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.place2book.com/da/sw2/sales/94d5gdf6ry/sale_closed"))
            startActivity(i)
        }

        val button2 = binding.programButton
        button2.setOnClickListener {


        }

        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}