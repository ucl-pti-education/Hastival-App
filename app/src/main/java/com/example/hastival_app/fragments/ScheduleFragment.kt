package com.example.hastival_app.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.hastival_app.MyAdapter
import com.example.hastival_app.R
import com.example.hastival_app.User
import com.example.hastival_app.databinding.FragmentScheduleBinding


class ScheduleFragment : Fragment() {

    private var _binding: FragmentScheduleBinding? = null
    private lateinit var userArrayList: ArrayList<User>

    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentScheduleBinding.inflate(inflater, container, false)

        val button1 = binding.link1
        button1.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vingardmusic.com/?fbclid=IwAR1ZfxpIby7-waZD6Wlhi45TTN-StRZDx8XrPyqmQVsYAYJ1TpqPIHq2s9A"))
            startActivity(i)
        }

        val button2 = binding.link2
        button2.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://open.spotify.com/artist/5FyteW1LsLiYfvRcDaOeT3"))
            startActivity(i)
        }

        val button3 = binding.link3
        button3.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://open.spotify.com/artist/4bGIddL6QajlPSilS7sNh8"))
            startActivity(i)
        }

        val button4 = binding.link4
        button4.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/mattis.vural"))
            startActivity(i)
        }

        val button5 = binding.link5
        button5.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/baandofficiel"))
            startActivity(i)
        }

        val button6 = binding.link6
        button6.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/HolmenHustlers/about"))
            startActivity(i)
        }

        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val imageId = intArrayOf(

            R.drawable.ic_baseline_home_24,
            R.drawable.ic_baseline_home_24,
            R.drawable.ic_baseline_home_24
        )

        val name = arrayOf(

            "John",
            "Mark",
            "Sergio"
        )

        val lastMessage = arrayOf(

            "yo",
            "how hanging",
            "whats up"
        )

        val lastMsgTime = arrayOf(

            "7.00 pm",
            "8.20 pm",
            "9.50 pm"
        )

        userArrayList = ArrayList()

        for (i in name.indices) {

            val user = User(name[i], lastMessage[i], lastMsgTime[i], imageId[i])
            userArrayList.add(user)

        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //binding.saturdayList.adapter = MyAdapter(requireContext(), userArrayList)
    }

}