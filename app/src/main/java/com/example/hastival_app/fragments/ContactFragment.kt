package com.example.hastival_app.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.hastival_app.R
import com.example.hastival_app.User
import com.example.hastival_app.databinding.FragmentContactBinding
import com.example.hastival_app.databinding.FragmentScheduleBinding
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.*
import kotlinx.android.synthetic.main.fragment_contact.*

class ContactFragment : Fragment(){

    private var _binding: FragmentContactBinding? = null

    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentContactBinding.inflate(inflater, container, false)

        val button = binding.facebookButton
        button.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/haastival/?fref=ts"))
            startActivity(i)
        }

        val button2 = binding.websiteButton
        button2.setOnClickListener {

            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.haastival.dk/"))
            startActivity(i)
        }

        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}