package com.example.hastival_app

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

class MyAdapter(context: Context, private val arrayList: ArrayList<User>) : ArrayAdapter<User>(context, R.layout.list_item, arrayList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val inflater : LayoutInflater = LayoutInflater.from(context)
        val view : View = inflater.inflate(R.layout.list_item, null)

        val imageView : ImageView = view.findViewById(R.id.artistPicture)
        val username : TextView = view.findViewById(R.id.artistTitle)
        val lastMsg : TextView = view.findViewById(R.id.artistDesc)
        val lastMsgTime : TextView = view.findViewById(R.id.scheduleSaturday)

        imageView.setImageResource(arrayList[position].artistImage)
        username.text = arrayList[position].artistName
        lastMsg.text = arrayList[position].artistDescription
        lastMsgTime.text = arrayList[position].scheduleTime

        return view
    }
}